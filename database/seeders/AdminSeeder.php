<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->last_name = 'Admin';
        $admin->middle_name = 'Admin';
        $admin->first_name = 'Admin';
        $admin->gender = 'Male';
        $admin->section = 'Admin';
        $admin->year_level = 'Admin';
        $admin->course_id = '1';
        $admin->email = 'admin@admin.com';
        $admin->save();
        $admin->attachRole('admin');
    }
}
