<?php

use App\Http\Controllers\CandidateController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ElectionController;
use App\Http\Controllers\PartyListController;
use App\Http\Controllers\VoterController;
use App\Http\Controllers\VoteSubmitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::group(['middleware' => ['canVote']], function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware(['auth']);

    Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
        Route::resource('voter', VoterController::class);
        Route::resource('election', ElectionController::class);
        Route::resource('candidate', CandidateController::class);
        Route::resource('partylist', PartyListController::class);
    });
    Route::group(['prefix' => 'student', 'middleware' => ['role:student']], function () {
        Route::post('submitvote', [VoteSubmitController::class, 'store'])->name('submitvote.store');
    });
});




require __DIR__ . '/auth.php';
