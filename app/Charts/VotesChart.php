<?php

declare(strict_types=1);

namespace App\Charts;

use App\Models\Election;
use App\Models\Postions;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class VotesChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $elections = Election::with('position.candidate.candidateInfo', 'position.candidate.partyList', 'position.candidate.voteCount')->where('status', 1)->first();
        // $positions =  $elections->position->pluck('positions');
        $positions = Postions::where('election_id', $elections->id)->pluck('positions')->toArray();


        return Chartisan::build()
            ->labels($positions)
            // ->dataset($positions, [1, 0, 0])
            ->dataset($positions, [0, 2, 2]);
        // ->dataset();
    }
}
