<?php

namespace App\Http\Middleware;

use App\Models\Election;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CanVoteUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->isAn('student')) {

                if (Auth::user()->status == 1) {
                    Auth::logout();
                    return redirect()->route('login')->withErrors('You can only Vote once');
                }

                $election = Election::where('status', 1)->first();

                if (!$election) {

                    Auth::logout();
                    return redirect()->route('login')->withErrors('No election is Open');
                }

                if ($election->typeOfVoters == "All Student") return $next($request);

                if ($election->typeOfVoters == Auth::user()->course->description) return $next($request);

                if ($election->typeOfVoters == Auth::user()->course->college->description)  return $next($request);

                Auth::logout();
                return redirect()->route('login')->withErrors('The election that is Open is not for you');
            }
        }
        return $next($request);
    }
}
