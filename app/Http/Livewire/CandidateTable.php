<?php

namespace App\Http\Livewire;

use App\Models\Candidate;
use Livewire\Component;

class CandidateTable extends Component
{
    public function render()
    {
        $candidates = Candidate::with('election', 'position', 'candidateInfo')->get();
        return view('livewire.candidate-table', compact('candidates'));
    }
}
