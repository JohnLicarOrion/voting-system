<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class VoterTable extends Component
{
    public function render()
    {
        $voters = User::with('course')->whereRoleIs('student')->get();
        return view('livewire.voter-table', compact('voters'));
    }
}
