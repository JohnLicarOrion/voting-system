<?php

namespace App\Http\Livewire;

use App\Models\Election;
use App\Models\Postions;
use Livewire\Component;

class CandidatePosition extends Component
{

    public $selectedElection = null;
    public $selectedPosition = null;
    public $positions = [];

    public function render()
    {
        return view(
            'livewire.candidate-position',
            [
                'elections' => Election::where('status', 0)->get()
            ],
        );
    }

    public function updatedSelectedElection($elections_id)
    {
        $this->positions = Postions::where('election_id', $elections_id)->get();
    }
}
