<?php

namespace App\Http\Livewire;

use App\Models\Election;
use App\Models\Postions;
use Livewire\Component;

class EditPosition extends Component
{
    public $positions = [];

    public function mount(Election $election)
    {

        $postions = Postions::with('election')->where('election_id', $election->id)->get();

        foreach ($postions as $position) {
            $this->positions[] =
                [
                    'id' => $position->id,
                    'position' => $position->positions
                ];
        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function addPosition()
    {
        $this->positions[] = ['position' => ''];
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function removePosition($index)
    {
        unset($this->positions[$index]);
        $this->positions = array_values($this->positions);
    }

    public function render()
    {
        info($this->positions);
        return view('livewire.edit-position');
    }
}
