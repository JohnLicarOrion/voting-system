<?php

namespace App\Http\Livewire;

use App\Models\Election;
use Livewire\Component;

class VoteList extends Component
{
    public function render()
    {
        $elections = Election::with('position.candidate.candidateInfo', 'position.candidate.partyList', 'position.candidate.voteCount')->where('status', 1)->first();
        // dd($elections);
        return view('livewire.vote-list', compact('elections'));
    }
}
