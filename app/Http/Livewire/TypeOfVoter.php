<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\College;
use App\Models\Course;

class TypeOfVoter extends Component
{
    public $whoCanVote = 'All Student';
    public $typeOfVoter;
    public function render()
    {
        $colleges = College::get();
        $courses = Course::get();
        return view('livewire.type-of-voter', compact('colleges', 'courses'));
    }
}
