<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class CandidateSearch extends Component
{

    public $query;
    public $candidate = [];

    public function mount()
    {
        $this->query = '';
        $this->candidate = '';
    }

    public function updatedQuery()
    {
        $this->candidate = User::with('course')->where('id',  $this->query)
            ->first();
    }

    public function render()
    {
        info($this->candidate);
        return view('livewire.candidate-search');
    }
}
