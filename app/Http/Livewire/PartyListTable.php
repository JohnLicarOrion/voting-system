<?php

namespace App\Http\Livewire;

use App\Models\PartyList;
use Livewire\Component;

class PartyListTable extends Component
{
    public function render()
    {

        return view(
            'livewire.party-list-table',
            [
                'partylists' => PartyList::all()
            ]
        );
    }
}
