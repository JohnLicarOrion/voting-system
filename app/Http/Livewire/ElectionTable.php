<?php

namespace App\Http\Livewire;

use App\Models\Election;
use Livewire\Component;

class ElectionTable extends Component
{

    public function render()
    {
        $elections = Election::orderBy('id', 'desc')->get();
        return view('livewire.election-table', compact('elections'));
    }
}
