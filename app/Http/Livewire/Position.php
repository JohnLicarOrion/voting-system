<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Position extends Component
{
    public $positions = [];

    public function mount()
    {
        $this->positions = [
            [
                'position' => 'President'

            ],
            [
                'position' => 'Vice-President'
            ],
            [
                'position' => 'Secretary'
            ],
        ];
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function addPosition()
    {
        $this->positions[] = ['position' => ''];
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function removePosition($index)
    {
        unset($this->positions[$index]);
        $this->positions = array_values($this->positions);
    }

    public function render()
    {
        info($this->positions);
        return view('livewire.position');
    }
}
