<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartyListRequest;
use App\Models\PartyList;
use Illuminate\Http\Request;

class PartyListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.admin.partylist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.admin.partylist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartyListRequest $request)
    {
        PartyList::create($request->validated());
        return redirect()->route('partylist.index')->with('message', 'Party List Added successfully');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PartyList  $partyList
     * @return \Illuminate\Http\Response
     */
    public function edit(PartyList $partylist)
    {
        return view('users.admin.partylist.edit', compact('partylist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PartyList  $partyList
     * @return \Illuminate\Http\Response
     */
    public function update(PartyListRequest $request, PartyList $partylist)
    {
        $partylist->update($request->validated());
        return redirect()->route('partylist.index')->with('message', 'Party List Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PartyList  $partyList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PartyList $partylist)
    {
        $partylist->delete();
        return redirect()->route('partylist.index')->with('message', 'Party List Succesfully Removed');
    }
}
