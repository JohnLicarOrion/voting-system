<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\Election;
use App\Models\Postions;
use App\Models\VoteCount;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {



        if (Auth::user()->isAn('admin')) return view('users.admin.dashboard');

        if (Auth::user()->isAn('student')) {

            $elections = Election::where('status', 1)->first();
            return view('users.voter.dashboard', compact('elections'));
        }

        if (Auth::user()) return view('auth.login');
    }
}
