<?php

namespace App\Http\Controllers;

use App\Models\Postions;
use Illuminate\Http\Request;

class PostionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Postions  $postions
     * @return \Illuminate\Http\Response
     */
    public function show(Postions $postions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Postions  $postions
     * @return \Illuminate\Http\Response
     */
    public function edit(Postions $postions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Postions  $postions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postions $postions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Postions  $postions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postions $postions)
    {
        //
    }
}
