<?php

namespace App\Http\Controllers;

use App\Models\Election;
use App\Models\User;
use App\Models\VoteCount;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoteSubmitController extends Controller
{
    public function store(Request $request)
    {

        $elections = Election::with('position.candidate.candidateInfo', 'position.candidate.partyList')->where('status', 1)->first();

        $vote = collect();

        foreach ($request->except('_token') as $index) {
            $vote->push(
                [
                    'candidate_id'   => $index[0],
                    'election_id'   => $elections->id,
                    'voter_id'      => auth()->user()->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]
            );
        }
        VoteCount::insert($vote->toArray());
        User::where('id', auth()->user()->id)
            ->update(['status' => 1]);
        Auth::logout();
        return redirect('login')->with('message', 'Vote Submitted');
    }
}
