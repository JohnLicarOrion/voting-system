<?php

namespace App\Http\Controllers;

use App\Http\Requests\ElectionRequest;

use App\Models\Election;
use App\Models\Postions;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\PostDec;

class ElectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.admin.election.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('users.admin.election.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ElectionRequest $request)
    {

        $election = Election::create($request->validated());
        foreach ($request->positions as $position) {
            Postions::updateOrCreate(
                [
                    'positions' => $position['position'],
                    'election_id' => $election->id
                ],
            );
        }
        return redirect()->route('election.index')->with('message', 'Election Added successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function edit(Election $election)
    {
        return view('users.admin.election.edit', compact('election'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function update(ElectionRequest $request, Election $election)
    {


        $election->update($request->validated());
        $election->where('id', '!=', $election->id)->update(['status' => 0]);
        if ($request->positions) {
            foreach ($request->positions as $position) {
                Postions::updateOrCreate(
                    [
                        'positions' => $position['position'],
                        'election_id' => $election->id
                    ],
                );
            }
        }
        return redirect()->route('election.index')->with('message', 'Election Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function destroy(Election $election)
    {
        $election->delete();
        return redirect()->route('election.index')->with('message', 'Election Deleted successfully');
    }
}
