<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoterRequest;
use App\Models\Course;
use App\Models\User;
use Illuminate\Support\Facades\File;

class VoterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.admin.voters.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::get();
        return view('users.admin.voters.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VoterRequest $request)
    {


        $voter = User::create($request->validated());
        $voter->attachRole('student');

        if ($request->hasFile('avatar')) {
            $avatar =  $voter->id . '-' . $request->first_name . '-' . $request->last_name . '.' . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('images/profile'), $avatar);
            $voter->update(['avatar' => $avatar]);
        }

        return redirect()->route('voter.index')->with('message', 'Voter Added successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $voter)
    {
        $courses = Course::get();
        return view('users.admin.voters.edit', compact('voter', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VoterRequest $request, User $voter)
    {

        unlink("images/profile/" . $voter->avatar);
        $voter->update($request->validated());
        if ($request->hasFile('avatar')) {
            $avatar =  $voter->id . '-' . $request->first_name . '-' . $request->last_name . '.' . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('images/profile'), $avatar);
            $voter->update(['avatar' => $avatar]);
        }
        return redirect()->route('voter.index')->with('message', 'Teacher Profile Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $voter)
    {
        $voter->delete();
        File::delete('images/profile/' . $voter->avatar);
        return redirect()->route('voter.index')->with('message', 'Teacher Profile Deleted successfully');
    }
}
