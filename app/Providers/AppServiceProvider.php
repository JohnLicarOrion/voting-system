<?php

namespace App\Providers;

use Carbon\Carbon;
use ConsoleTVs\Charts\Registrar as Charts;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        $charts->register([
            \App\Charts\VotesChart::class,
        ]);
        Carbon::macro('toFormatedDate', function () {
            return $this->format('F j, Y  h:i:s A');
        });
    }
}
