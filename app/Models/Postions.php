<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postions extends Model
{
    use HasFactory;

    protected $fillable = [
        'positions',
        'election_id',
    ];

    public function election()
    {
        return $this->belongsTo(Election::class);
    }
    public function candidate()
    {
        return $this->hasMany(Candidate::class, 'position_id');
    }
}
