<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoteCount extends Model
{
    use HasFactory;
    protected $fillable = [
        'candidate_id',
        'election_id',
        'voter_id'
    ];

    public function election()
    {
        return $this->belongsTo(Election::class);
    }
    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }
}
