<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $dates = ['start', 'end'];
    protected $fillable = [
        'name',
        'status',
        'typeOfVoters',
        // 'start',
        // 'end',
    ];

    public function position()
    {
        return $this->hasMany(Postions::class, 'election_id');
    }
}
