<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'election_id',
        'position_id',
        'party_list_id',
    ];

    public function election()
    {
        return $this->belongsTo(Election::class);
    }

    public function position()
    {
        return $this->belongsTo(Postions::class);
    }

    public function candidateInfo()
    {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function partyList()
    {
        return $this->belongsTo(PartyList::class);
    }

    public function voteCount()
    {
        // return $this->hasMany(VoteCount::class, 'candidate_id');
        return $this->hasMany(VoteCount::class)->selectRaw('candidate_id, count(*) as count')->groupBy('candidate_id');
    }
}
