<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Create New Voter') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 bg-white border-b border-gray-200">


          <x-auth-validation-errors />
          <form method="POST" action="{{ route('voter.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="grid grid-cols-3 gap-6">

              <div>
                <x-label for="name" :value="__('First Name')" />
                <x-input id="first_name" class="block mt-1 w-full" type="text" name="first_name"
                  :value="old('first_name')" autofocus />
              </div>

              <div>
                <x-label for="middle_name" :value="__('Middle Name')" />
                <x-input id="middle_name" class="block mt-1 w-full" type="text" name="middle_name"
                  :value="old('middle_name')" />
              </div>

              <div>
                <x-label for="last_name" :value="__('Last Name')" />
                <x-input id="last_name" class="block mt-1 w-full" type="text" name="last_name"
                  :value="old('last_name')" />
              </div>

              <div>
                <x-label for="gender" :value="__('Gender')" />
                <select id="gender" name="gender" :value="old('gender')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">


                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                  <option value="Others">Others</option>

                </select>
              </div>

              <div>
                <x-label for="section" :value="__('Section')" />
                <x-input id="section" class="block mt-1 w-full" type="text" name="section" :value="old('section')" />
              </div>

              <div>
                <x-label for="year_level" :value="__('Year Level')" />
                <select id="year_level" name="year_level" :value="old('year_level')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                  <option value="First Year">First Year</option>
                  <option value="Second Year">Second Year</option>
                  <option value="Third Year">Third Year</option>
                  <option value="Fourth Yea">Fourth Year</option>
                </select>
              </div>

              <div>
                <x-label for="course_id" :value="__('Course')" />
                <select id="course_id" name="course_id" :value="old('course_id')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">


                  @foreach ($courses as $course)
                  <option value="{{$course->id }}">{{ $course->description }}</option>
                  @endforeach

                </select>
              </div>

              <div>
                <x-label for="email" :value="__('Email')" />
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" />
              </div>

              <div>
                <x-label for="avatar" :value="__('Image')" />
                <x-input id="avatar" class="block mt-1 w-full" type="file" name="avatar" :value="old('avatar')" />
              </div>

            </div>
            <div class="flex items-center justify-end mt-4">
              <x-button class="ml-3">
                {{ __('Create') }}
              </x-button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
