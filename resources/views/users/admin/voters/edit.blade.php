<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Update Voter Information') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 bg-white border-b border-gray-200">


          <x-auth-validation-errors />
          <form method="POST" action="{{ route('voter.update', $voter ) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="grid grid-cols-3 gap-6">

              <div>
                <x-label for="name" :value="__('First Name')" />
                <x-input id="first_name" class="block mt-1 w-full" type="text" name="first_name"
                  value="{{ $voter->first_name }}" autofocus />

              </div>

              <div>
                <x-label for="middle_name" :value="__('Middle Name')" />
                <x-input id="middle_name" class="block mt-1 w-full" type="text" name="middle_name"
                  value="{{ $voter->middle_name }}" />
              </div>

              <div>
                <x-label for="last_name" :value="__('Last Name')" />
                <x-input id="last_name" class="block mt-1 w-full" type="text" name="last_name"
                  value="{{ $voter->last_name }}" />
              </div>

              <div>
                <x-label for="gender" :value="__('Gender')" />
                <select id="gender" name="gender" :value="old('gender')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                  <option value="Male" @if ($voter->gender === "Male" ) selected @endif>Male</option>
                  <option value="Female" @if ($voter->gender === "Female" ) selected @endif>Female</option>
                  <option value="Other" @if ($voter->gender === "Others" ) selected @endif>Other</option>
                </select>
              </div>

              <div>
                <x-label for="section" :value="__('Section')" />
                <x-input id="section" class="block mt-1 w-full" type="text" name="section"  value="{{ $voter->section }}" />
              </div>

              <div>
                <x-label for="year_level" :value="__('Year Level')" />
                <select id="year_level" name="year_level" :value="old('year_level')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                  <option value="First Year" @if ($voter->year_level === "First Year" ) selected @endif>First Year</option>
                  <option value="Second Year" @if ($voter->year_level === "Second Year" ) selected @endif>Second Year</option>
                  <option value="Third Year"  @if ($voter->year_level === "Third Year" ) selected @endif>Third Year</option>
                  <option value="Fourth Year" @if ($voter->year_level === "Fourth Year" ) selected @endif>Fourth Year</option>
                </select>
              </div>

              <div>
                <x-label for="course_id" :value="__('Course')" />
                <select id="course_id" name="course_id" :value="old('course_id')"
                  class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">


                  @foreach ($courses as $course)
                  <option value="{{$course->id }}">{{ $course->description }}</option>
                  @endforeach

                </select>
              </div>

              <div>
                <x-label for="email" :value="__('Email')" />
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ $voter->email }}" />

              </div>

              <div>
                <x-label for="avatar" :value="__('Image')" />
                <div class="flex">
                  @if (!$voter->avatar)
                  <img src="{{ Avatar::create($voter->first_name . ' ' . $voter->last_name)->toBase64() }}"
                    class="mx-2" />
                  @else
                  <img src="{{asset('images/profile/'. $voter->avatar)}}" alt="Teacher Picture"
                    class="w-12 h-12 border dark:border-coolGray-700">
                  @endif
                  <x-input id="avatar" class="block ml-3 mt-1 w-full" type="file" name="avatar"
                    :value="old('avatar')" />
                </div>

              </div>

            </div>
            <div class="flex items-center justify-end mt-4">
              <x-button class="ml-3">
                {{ __('Update') }}
              </x-button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
