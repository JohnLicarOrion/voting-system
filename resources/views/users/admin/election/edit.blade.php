<x-app-layout>
    <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Update Election') }}
      </h2>
    </x-slot>

    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
          <div class="p-6 bg-white border-b border-gray-200">


            <x-auth-validation-errors />
            <form method="POST" action="{{ route('election.update', $election) }}">
                @method('PUT')
              @csrf
             @if ($election->status == 1)

                <div>
                    <x-input id="name" class="block mt-1 w-full" type="hidden" name="name"
                    value="{{ $election->name }}" autofocus />
                    <x-label for="status" :value="__('Status')" />
                    <select id="status" name="status" :value="old('status')"
                      class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                      <option value=1 @if ($election->status === 1 ) selected @endif>Open Election</option>
                      <option value=0 @if ($election->status === 0 ) selected @endif>Close Election</option>

                    </select>
                </div>

             @else
             <div class="grid grid-cols-1">

                <div>
                  <x-label for="name" :value="__('Elecition Name')" />
                  <x-input id="name" class="block mt-1 w-full" type="text" name="name"
                  value="{{ $election->name }}" autofocus />
                </div>

              </div>
                <p class=" pt-5 font-semibold ">Who can vote</p>
                @livewire('type-of-voter')

              <div class="grid grid-cols-2 gap-6 mt-5">

                    <div>
                        <x-label for="status" :value="__('Status')" />
                        <select id="status" name="status" :value="old('status')"
                          class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                          <option value=1 @if ($election->status === 1 ) selected @endif>Open Election</option>
                          <option value=0 @if ($election->status === 0 ) selected @endif>Close Election</option>

                        </select>
                    </div>
              </div>

              <div >
                @livewire('edit-position', ['election' => $election])
            </div>
             @endif

              <div class="flex items-center justify-end mt-4">
                <x-back-button href="{{ route('election.index') }}" class="ml-3">
                    {{ __('Back') }}
                  </x-back-button>
                <x-button class="ml-3">
                  {{ __('Update') }}
                </x-button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </x-app-layout>
