<x-app-layout>
    <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Party List') }}
      </h2>
    </x-slot>

    <div class="py-12">
      <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-x-auto shadow-sm sm:rounded-lg">
          <div class="relative flex space-x-4  my-6 mx-6">
            <a href="{{ route('partylist.create') }}"
              class="bg-purple-400 hover:bg-purple-600 text-white py-2 px-4 rounded">Add
              Party List</a>
            <x-input wire:model.debounce.300ms="search" id="search" class="absolute right-0 w-1/3" type="search"
              name="search" placeholder="Search Party List" :value="old('search')" />
          </div>


          <div class="p-6 bg-white border-b border-gray-200">
            <div x-data="{ show: true }" x-show.transition.opacity.out.duration.2000ms="show"
              x-init="setTimeout(() => show = false, 3000)">
              <x-success-message />
            </div>

            <!-- component -->
            @livewire('party-list-table')
          </div>
        </div>
      </div>
    </div>
  </x-app-layout>
