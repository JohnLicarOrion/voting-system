<x-app-layout>
    <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Create New Candidate') }}
      </h2>
    </x-slot>

    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
          <div class="p-6 bg-white border-b border-gray-200">
            <x-auth-validation-errors />
            <form method="POST" action="{{ route('candidate.store') }}">
              @csrf
              @livewire('candidate-search')
              @livewire('candidate-position')
              <div class="grid grid-cols-1 gap-6 mt-5">
                    <div>
                        <x-label for="party_list_id" :value="__('Party List')" />
                        <select id="party_list_id" name="party_list_id" :value="old('party_list_id')"
                        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">

                        <option value="">Select a Party List</option>
                                @foreach ($partylists as $partylist)
                                <option value="{{ $partylist->id }}">{{ $partylist->name }}</option>
                                @endforeach
                        </select>
                    </div>


              </div>

                <div class="flex items-center justify-end mt-4">
                  <x-back-button href="{{ route('election.index') }}"  class="ml-3">
                      {{ __('Back') }}
                  </x-back-button>
                <x-button class="ml-3">
                  {{ __('Create') }}
                </x-button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </x-app-layout>
