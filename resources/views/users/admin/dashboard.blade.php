<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Dashboard') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 bg-white border-b border-gray-200">
          {{-- <!-- Chart's container -->
         <div id="chart" style="height: 300px;"></div> --}}

        <!-- component -->
        @livewire('vote-list')
        </div>
      </div>
    </div>
  </div>

  @push('script')
   <!-- Your application script -->
   <script>
    const chart = new Chartisan({
      el: '#chart',
      url: "@chart('votes_chart')",
      hooks: new ChartisanHooks()
        .colors()
        .responsive()
        .beginAtZero()
        .legend({ position: 'bottom' })
        .title('This is a sample chart using chartisan!')

    });
  </script>
  @endpush
</x-app-layout>
