<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-success-message />
            <form method="POST" action="{{ route('submitvote.store') }}">
                @csrf
            <div class="grid grid-cols-2 gap-4 justify-center">
                    @foreach ($elections->position as  $index => $position)
                        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg border-2 border-blue-500">
                                <div class="py-3 text-white flex justify-center text-2xl bg-blue-500">
                                    {{  $position->positions }}
                                </div>
                                <div class="grid grid-cols-2 gap-4 pt-5">
                                    @foreach ($position->candidate as $candidate)
                                    <div class="mb-5 text-current">
                                        <div class="flex justify-center">
                                            @if (!$candidate->candidateInfo->avatar)
                                            <img class="mx-5"
                                                src="{{ Avatar::create($candidate->candidateInfo->first_name . ' ' . $candidate->candidateInfo->last_name)->toBase64() }}" />
                                            @else
                                                <img class="w-36 h-36 rounded-lg mx-5"
                                                src="{{ asset('images/profile/'.$candidate->candidateInfo->avatar) }}" alt="Voter Profile" />
                                             @endif
                                        </div>
                                        <div class="flex justify-center">
                                            <p class="font-bold text-base">Name: {{ $candidate->candidateInfo->first_name . ' ' .$candidate->candidateInfo->last_name }}</p>
                                        </div>

                                        <div class="flex justify-center">
                                            <p class="font-bold text-base">Year Level: {{ $candidate->candidateInfo->year_level }}</p>
                                        </div>

                                        <div class="flex justify-center">
                                            <p class="font-bold text-base">Party List: {{ optional($candidate->partyList)->name }}</p>
                                        </div>
                                        <div class="flex justify-center">

                                            <input type="checkbox" name="{{ $position->positions }}[]"  value="{{ $candidate->id }}" class="appearance-none checked:bg-blue-600 checked:border-transparent"/>
                                        </div>
                                    </div>
                                @endforeach
                                </div>

                        </div>
                    @endforeach

            </div>
            <x-button class=" mt-5">
                {{ __('Submit') }}
              </x-button>
        </form>
        </div>
    </div>
    @push('script')

    <script>
       $('input[type="checkbox"]').on('change', function() {
            $('input[name="' + this.name + '"]').not(this).prop('checked', false);
        });
    </script>

    @endpush
</x-app-layout>
