<div>
    <div class="grid grid-cols-1 gap-6 mt-5">
        <div>
            <x-label for="student_id" :value="__('Student ID')" />
            <x-input id="student_id" class="block mt-1 w-full" type="text" name="student_id"
            wire:model="query"
            :value="old('student_id')" />
        </div>
    </div>
    @if (!empty($candidate))
        @if ($candidate->first_name != 'Admin')
        <div class="grid grid-cols-2 gap-6 mt-5">

            <div>
                <x-label for="name" :value="__('Candidate Name')" />
                <x-input id="name" class="block mt-1 w-full" type="text" name="name"  :value="old('name')" readonly
              value="{{ $candidate->first_name . ' ' . $candidate->last_name}}"
            />
            </div>
            <div>
                <x-label for="section" :value="__('Section')" />
                <x-input id="section" class="block mt-1 w-full" type="text" name="section" :value="old('section')" readonly
                value="{{ $candidate->section}}" />
            </div>
            <div>
                <x-label for="year_level" :value="__('Year Level')" />
                <x-input id="year_level" class="block mt-1 w-full" type="text" name="year_level" :value="old('year_level')" readonly
                value="{{ $candidate->year_level}}" />
            </div>
            <div>
                <x-label for="course" :value="__('Course')" />
                <x-input id="course" class="block mt-1 w-full" type="text" name="course" :value="old('course')" readonly
                value="{{ $candidate->course->description}}" />
            </div>
        </div>
        @endif
    @endif
</div>

