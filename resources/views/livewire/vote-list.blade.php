<div class="overflow-x-auto" >
    <div class="w-full">
        @if (!$elections == null)
            <div class="grid grid-cols-2 gap-4">

                    @forelse ($elections->position as $position)
                    <div class="bg-white shadow-md rounded  border-4  border-opacity-100">
                        <div class="text-2xl">{{ $position->positions }}</div>
                        <table class="min-w-max w-full table-auto">
                            <thead>
                                <tr class="bg-gray-200 text-gray-600 uppercase text-md leading-normal">
                                    <th class="py-3 px-6 text-left">Candidate Name</th>
                                    <th class="py-3 px-6 text-center">Vote Count</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 text-base font-light">
                                @foreach ($position->candidate as $candidate)
                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                    <td class="py-3 px-6 text-left">
                                        <div class="flex items-center">
                                        <div class="mr-5">
                                            @if (!$candidate->candidateInfo->avatar)
                                            <img
                                                src="{{ Avatar::create($candidate->candidateInfo->first_name . ' ' . $candidate->candidateInfo->last_name)->toBase64() }}" />
                                            @else
                                            <img class="w-6 h-6 rounded-full"
                                                src="{{ asset('images/profile/'.$candidate->candidateInfo->avatar) }}" alt="Voter Profile" />
                                            @endif
                                        </div>
                                        <span>{{ $candidate->candidateInfo->first_name . ' ' .  $candidate->candidateInfo->last_name}}</span>
                                    </div>
                                    </td>
                                    <td class="py-3 px-6 text-center">
                                        <span>{{ $candidate->voteCount[0]->count}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    @empty
                    <td>
                        No Information to Display
                      </td>
                    @endforelse
                </div>
                    @else
                    <td>
                        <div class="flex justify-center">
                            <div>
                                <img class="h-60" src="{{ asset('images/icons/undraw_void_3ggu.png') }}" alt="">
                            <div class="pl-10">
                                No Open Election
                            </div>
                            </div>
                        </div>
                      </td>
                    @endif


    </div>
</div>
