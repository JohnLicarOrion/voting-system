<div>
        <div class="flex items-center justify-end my-4">
            <x-add-position-button class="ml-3 cursor-pointer" wire:click.prevent="addPosition()">
                {{ __('Add Position') }}
            </x-add-position-button>
        </div>

        <table class="min-w-max w-full table-fixed">
            <thead>
                <tr class="bg-gray-200 text-gray-600 uppercase text-md leading-normal">
                    <th class="py-3 px-6 text-left w-5/6">Positions</th>
                    <th class="py-3 px-6 text-left w-1/6">Actions</th>
                </tr>
            </thead>
            <tbody class="text-base font-light">
                @foreach ($positions as $index => $position)
                    <div>
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                <x-input id="position" class="block mt-4 w-full" type="text" name="positions[{{$index}}][position]"
                                        wire:model="positions.{{$index}}.position"
                                        />
                            </td>
                            <td class="py-3 px-6 text-center">
                                    <div class="w-4 mr-2 mt-4 transform hover:text-red-500 hover:scale-110">
                                            <button class="w-5 h-5" wire:click.prevent="removePosition({{$index}})">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 1 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                </svg>
                                            </button>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    </div>
                @endforeach
            </tbody>
        </table>
</div>



