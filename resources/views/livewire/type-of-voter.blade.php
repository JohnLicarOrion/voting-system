<div>
<div class="grid grid-cols-2 gap-6 mt-5 ">
    <input type="hidden" id="typeOfVoters" name="typeOfVoters" wire:model="whoCanVote">
        <div>
            <x-label for="voter" :value="__('Who can vote?')" />
            <select id="voter" name="voter"  wire:model="typeOfVoter"
                class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                <option value="All Student">All Student</option>
                <option value="College">Per College</option>
                <option value="Course">Per Course</option>
            </select>
        </div>

      @if($typeOfVoter == "College")
        <div>
            <x-label for="collegeVoter" :value="__('What college?')" />
            <select id="collegeVoter" name="collegeVoter" :value="old('collegeVoter')"
            class="block mt-1 w-full rounded-md shadow-sm border-gray-300
            focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            wire:model="whoCanVote">
            <option value="">Select College</option>
                @foreach ($colleges as $college)
                    <option value="{{ $college->description }}">{{ $college->description }}</option>
                @endforeach
            </select>
        </div>
        @endif

        @if ($typeOfVoter == "Course")
            <div>
                <x-label for="collegeVoter" :value="__('What course?')" />
                <select id="collegeVoter" name="collegeVoter" :value="old('collegeVoter')"
                class="block mt-1 w-full rounded-md shadow-sm border-gray-300
                focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                wire:model="whoCanVote">
                <option value="">Select Course</option>
                    @foreach ($courses as $course)
                        <option value="{{ $course->description }}">{{ $course->description }}</option>
                    @endforeach
                </select>
            </div>
        @endif


    </div>
</div>
