<div class="grid grid-cols-2 gap-6 mt-5">

       <div>
        <x-label for="election_id" :value="__('Election Name')" />
        <select id="election_id" name="election_id" :value="old('election_id')"
        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
        wire:model="selectedElection">
            <option value="">Select a Election</option>
                @foreach ($elections as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
        </select>

       </div>

        <div>
            <x-label for="position_id" :value="__('Position')" />
        <select id="position_id" name="position_id" :value="old('position_id')"
        class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
        wire:model="selectedPosition" >

        <option value="">Select a Position</option>
                @foreach ($positions as $position)
                <option value="{{ $position->id }}">{{ $position->positions }}</option>
                @endforeach
        </select>
        </div>
</div>
